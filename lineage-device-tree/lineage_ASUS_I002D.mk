#
# Copyright (C) 2024 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

# Inherit from those products. Most specific first.
$(call inherit-product, $(SRC_TARGET_DIR)/product/core_64_bit.mk)
$(call inherit-product, $(SRC_TARGET_DIR)/product/full_base_telephony.mk)

# Inherit some common Lineage stuff.
$(call inherit-product, vendor/lineage/config/common_full_phone.mk)

# Inherit from ASUS_I002D device
$(call inherit-product, device/asus/ASUS_I002D/device.mk)

PRODUCT_DEVICE := ASUS_I002D
PRODUCT_NAME := lineage_ASUS_I002D
PRODUCT_BRAND := asus
PRODUCT_MODEL := ASUS_I002D
PRODUCT_MANUFACTURER := asus

PRODUCT_GMS_CLIENTID_BASE := android-asus-tpin

PRODUCT_BUILD_PROP_OVERRIDES += \
    PRIVATE_BUILD_DESC="WW_I002D-user 12 SKQ1.210821.001 31.0210.0210.324 release-keys"

BUILD_FINGERPRINT := asus/WW_I002D/ASUS_I002D:12/SKQ1.210821.001/31.0210.0210.324:user/release-keys
