#
# Copyright (C) 2024 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

# Enable updating of APEXes
$(call inherit-product, $(SRC_TARGET_DIR)/product/updatable_apex.mk)

# A/B
PRODUCT_PACKAGES += \
    android.hardware.boot@1.2-impl \
    android.hardware.boot@1.2-impl.recovery \
    android.hardware.boot@1.2-service

PRODUCT_PACKAGES += \
    update_engine \
    update_engine_sideload \
    update_verifier

AB_OTA_POSTINSTALL_CONFIG += \
    RUN_POSTINSTALL_system=true \
    POSTINSTALL_PATH_system=system/bin/otapreopt_script \
    FILESYSTEM_TYPE_system=ext4 \
    POSTINSTALL_OPTIONAL_system=true

AB_OTA_POSTINSTALL_CONFIG += \
    RUN_POSTINSTALL_vendor=true \
    POSTINSTALL_PATH_vendor=bin/checkpoint_gc \
    FILESYSTEM_TYPE_vendor=ext4 \
    POSTINSTALL_OPTIONAL_vendor=true

PRODUCT_PACKAGES += \
    checkpoint_gc \
    otapreopt_script

# API levels
PRODUCT_SHIPPING_API_LEVEL := 29

# fastbootd
PRODUCT_PACKAGES += \
    android.hardware.fastboot@1.1-impl-mock \
    fastbootd

# Health
PRODUCT_PACKAGES += \
    android.hardware.health@2.1-impl \
    android.hardware.health@2.1-service

# Overlays
PRODUCT_ENFORCE_RRO_TARGETS := *

# Partitions
PRODUCT_USE_DYNAMIC_PARTITIONS := true

# Product characteristics
PRODUCT_CHARACTERISTICS := nosdcard

# Rootdir
PRODUCT_PACKAGES += \
    UTSdumpstate.sh \
    WiFiFWTool.sh \
    WifiMac.sh \
    WifiSARPower.sh \
    antennaSwitchSvc.sh \
    country.sh \
    cscclearlog.sh \
    ddr_info.sh \
    execkernelevt.sh \
    fp_version.sh \
    gsensor2_status.sh \
    gyroscope2_status.sh \
    init.asus.check_asdf.sh \
    init.asus.check_last.sh \
    init.asus.checkdatalog.sh \
    init.asus.checklogsize.sh \
    init.asus.gamemode.sh \
    init.asus.kernelmessage.sh \
    init.class_main.sh \
    init.crda.sh \
    init.mdm.sh \
    init.qcom.class_core.sh \
    init.qcom.coex.sh \
    init.qcom.early_boot.sh \
    init.qcom.efs.sync.sh \
    init.qcom.post_boot.sh \
    init.qcom.sdio.sh \
    init.qcom.sh \
    init.qcom.usb.sh \
    init.qti.chg_policy.sh \
    init.qti.dcvs.sh \
    init.qti.ims.sh \
    init.qti.media.sh \
    init.qti.qcv.sh \
    mount_apd.sh \
    parse.simcode.sh \
    procrankdump.sh \
    qca6234-service.sh \
    repartition.sh \
    savelogmtp.sh \
    savelogs.sh \
    savelogs_complete.sh \
    sensors_factory_init.sh \
    shipping_rework.sh \
    ssr_cfg.sh \
    tarevtlog.sh \
    touch_ver.sh \
    triggerpanic.sh \
    ufs_info.sh \
    update_threshold.sh \
    widevine.sh \

PRODUCT_PACKAGES += \
    fstab.qcom \
    init.asus.debugtool.rc \
    init.asus.usb.rc \
    init.qcom.factory.rc \
    init.qcom.rc \
    init.target.rc \
    init.target.wigig.rc \
    init.recovery.qcom.rc \

PRODUCT_COPY_FILES += \
    $(LOCAL_PATH)/rootdir/etc/fstab.qcom:$(TARGET_COPY_OUT_RAMDISK)/fstab.qcom

# Soong namespaces
PRODUCT_SOONG_NAMESPACES += \
    $(LOCAL_PATH)

# Inherit the proprietary files
$(call inherit-product, vendor/asus/ASUS_I002D/ASUS_I002D-vendor.mk)
