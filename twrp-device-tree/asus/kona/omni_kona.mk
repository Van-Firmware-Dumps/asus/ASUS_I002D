#
# Copyright (C) 2024 The Android Open Source Project
# Copyright (C) 2024 SebaUbuntu's TWRP device tree generator
#
# SPDX-License-Identifier: Apache-2.0
#

# Inherit from those products. Most specific first.
$(call inherit-product, $(SRC_TARGET_DIR)/product/core_64_bit.mk)
$(call inherit-product, $(SRC_TARGET_DIR)/product/full_base_telephony.mk)

# Inherit some common Omni stuff.
$(call inherit-product, vendor/omni/config/common.mk)

# Inherit from kona device
$(call inherit-product, device/asus/kona/device.mk)

PRODUCT_DEVICE := kona
PRODUCT_NAME := omni_kona
PRODUCT_BRAND := asus
PRODUCT_MODEL := ASUS_I002D
PRODUCT_MANUFACTURER := asus

PRODUCT_GMS_CLIENTID_BASE := android-asus-tpin

PRODUCT_BUILD_PROP_OVERRIDES += \
    PRIVATE_BUILD_DESC="WW_I002D-user 11 RKQ1.200710.002 30.60.112.269_20220614 release-keys"

BUILD_FINGERPRINT := asus/WW_I002D/ASUS_I002D:11/RKQ1.200710.002/30.60.112.269_20220614:user/release-keys
